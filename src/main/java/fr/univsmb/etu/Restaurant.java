package fr.univsmb.etu;

import java.util.*;

import javax.management.monitor.MonitorSettingException;

/**
 * 
 */
public class Restaurant {

    /**
     * 
     */
    public Restaurant() {
    	this.nom = "Bonne Franquette";
    	this.maCarte = new Carte();
    	this.listeServeur = new Vector();
    	this.listeTable = new Vector();
    }

    /**
     * 
     */
    public String nom;
    public Carte maCarte;
    public Vector<Table> listeTable;
    public Vector<Serveur> listeServeur;




    /**
     * 
     */
    public void consulterCarte() {
        // TODO implement here
    }

    /**
     * 
     */
    public String consulterPlat(String catego) {
        return maCarte.affichePlat(catego);
    }

    /**
     * 
     */
    public Vector<Menu> consulterMenus() {
        return maCarte.afficheMenus();
    }

	public String consulterMenuDuJour() {
		return maCarte.afficheMenuDuJour();
		
	}
	
	/**
     * @param integer nbClient 
     * @param integer nbTable
	 * @return 
     */
    public Table assignerTable(int nbClient,int nbTable) {
        Table t = listeTable.get(nbTable);
        t.setTable(nbClient, "Occupe");
        return t;
    }

    /**
     * @return 
     * 
     */
    public Vector<Serveur> afficheServeur() {
        return listeServeur;
    }

    /**
     * @param String nomServeur
     */
    public void connexion(String nomServeur) {
        // TODO implement here
    }

    /**
     * @param Table 1
     */
    public Commande creerCommande(Table t0) {
        return t0.creerCommande();
        
         
    }

    /**
	 * @return 
	 * 
	 */
	public Vector<Table> consulterCommande() {
		return this.listeTable;
	}

	/**
     * 
     */
    public void ajoutBoissonCommande() {
        // TODO implement here
    }

    /**
     * 
     */
    public void ajoutMenuCommande() {
        // TODO implement here
    }

    public void initRestaurant(){
    	maCarte.initCarte();
    	this.listeTable.addElement(new Table(1));
    	this.listeTable.add(new Table(2));
    	this.listeTable.add(new Table(3));
    	this.listeTable.add(new Table(4));
    	this.listeTable.add(new Table(5));
    	this.listeTable.add(new Table(6));
    	
    	this.listeServeur.add(new Serveur("Sophie","Serveur"));
    	listeServeur.get(0).listeTable.addElement(listeTable.get(0));
    	listeServeur.get(0).listeTable.addElement(listeTable.get(1));
    	listeServeur.get(0).listeTable.addElement(listeTable.get(2));
    	listeServeur.get(0).listeTable.addElement(listeTable.get(3));
    	this.listeServeur.add(new Serveur("Carlos","Cuisinier"));
    	this.listeServeur.add(new Serveur("Booba","Barman"));
    	this.listeServeur.add(new Serveur("Pierre","Patron"));
    	
    	
    }
    
    public void initRestaurant(String s){
    	
    	this.initRestaurant();
    	this.listeTable.get(0).creerCommande();
    	this.assignerTable(2, 0);
    	this.listeTable.get(0).c.ajoutMenu(this.maCarte.menuDuJour, 2);
    	//System.out.println(this.listeTable.get(0).c.listePlatsAPreparer);
    	Vector<Plat> listeBoisson = new Vector();
    	Vector<Plat> listeNourriture = new Vector();
    	
    	for (Plat plat : this.listeTable.get(0).c.listePlatsAPreparer) {
			if (plat instanceof Boisson) {
				listeBoisson.addElement(plat);
			}
			else {
				listeNourriture.add(plat);
			}
		}
    	this.listeTable.get(0).c.creerTicket(listeBoisson);
    	this.listeTable.get(0).c.creerTicket(listeNourriture);
    	this.listeTable.get(0).c.envoyerPlatSuivant();
    	
//    	System.out.print(this.listeTable.get(0).c.listeTicket.get(0).estBar());
//    	System.out.print(this.listeTable.get(0).c.listeTicket.get(1).estBar());
//    	System.out.print(this.listeTable.get(0).c.listeTicket.get(0).estCuisine());
//    	System.out.print(this.listeTable.get(0).c.listeTicket.get(1).estCuisine());
    	
    	
    	
    }

}