package fr.univsmb.etu;


import java.util.*;

/**
 * 
 */
public class Commande implements Observer {

	
	public EtatCommande etat;
	
	Ticket ticketCourant;
	
	public Vector<Plat> listePlatsAPreparer;
	
	public Vector<Plat> getListePlatsAPreparer() {
		return listePlatsAPreparer;
	}

	public Vector<Plat> listeCommandeAPayer;
	
	public Vector<Menu> listeMenu;
	
	public Vector<Ticket> listeTicket;
	
    /**
     * Default constructor
     */
    public Commande(){
    	listePlatsAPreparer = new Vector();
    	listeCommandeAPayer = new Vector();
    	listeMenu = new Vector();
    	listeTicket = new Vector();
    	etat = EtatCommande.EnAttente;
    }

    /**
     * 
     */
    public void ajoutPlat(Plat p,int qte){
    	this.etat = EtatCommande.EnCreation;
        for (int i = 0; i < qte; i++) {
			listeCommandeAPayer.add(p);
			listePlatsAPreparer.add(p);
		}
    }

    /**
     * @param qte 
     * 
     */
    public void ajoutMenu(Menu m, int qte){
        Vector<Plat> listePlat = m.getListePlat();
        for (int i = 0; i < qte; i++) {
        	this.majPlat(listePlat);
        	this.listeMenu.addElement(m);
		}
    }

    void majPlat(Vector<Plat> listePlat) {
		
		this.listePlatsAPreparer.addAll(listePlatsAPreparer.size(), listePlat);		
	}

	/**
     * 
     */
//    public void ajoutPlat(Vector<Plat> listePlat){
//        // TODO implement here
//    }

    /**
     * 
     */
//    public void ticketSuivant(){
//        // TODO implement here
//    }

    /**
     * 
     */
    public void envoyerPlatSuivant(){
//        for (Ticket ticket : listeTicket) {
//			if (ticket.etat == "en attente") {
//				ticketCourant = ticket;
//				ticket.changeEtatTicket("en pr�paration");
//				break;
//			}
//		}
    	this.actualisePrecedent();
    	for (Ticket ticket : listeTicket) {
			if (ticket.etat instanceof EtatTicketEnAttente){
				ticketCourant = ticket;
				ticketCourant.aEnvoyer();
				break;
			}
		}
    }

    private void actualisePrecedent() {
		if (ticketCourant != null) {
			ticketCourant.setEtatTicket(new EtatTicketEstRecuperer(ticketCourant));
		}
		else {
			etat = EtatCommande.EnPreparation;
		}
		
	}

	/**
     * 
     */
    public void setTicketCourant(){
        // TODO implement here
    }

	public Vector<Ticket> getListeTicket() {
		// TODO Auto-generated method stub
		return listeTicket;
	}

	public void creerTicket(Vector<Plat> listeRes) {
		Ticket t = new Ticket(listeRes);
		t.addObserver(this);
		this.listeTicket.addElement(t);
		
	}

	@Override
	public void update(Observable o, Object arg) {
		// TODO Auto-generated method stub
		if (this.listeTicket.lastElement().etat instanceof EtatTicketAVenirChercher) {
			this.envoyerPlatSuivant();
			etat = EtatCommande.EnAttentePayement;
		}
		
	}

	public double calculPrix() {
		double somme = 0.0;
		for (Menu menu : listeMenu) {
			somme += menu.prix;
		}
		for (Plat plat : listeCommandeAPayer) {
			somme += plat.prix;
		}
		return somme;
	}

}