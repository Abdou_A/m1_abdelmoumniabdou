package fr.univsmb.etu;

import java.util.*;

/**
 * 
 */
public class Carte {

    /**
     * 
     */
    public Carte() {
    	    	
    }


    public Menu menuDuJour;
    /**
     * 
     */
    public Vector<Menu> listeMenus;


    /**
     * 
     */
    public Vector<Plat> listePlats;


    public Vector<Plat> getListePlats() {
		return listePlats;
	}

	/**
     * 
     */
    public String affichePlat(String catego) {
    	
    	String entree = "\nEntr�e(s) : ";
    	String boisson = "\nBoisson(s) : ";
    	String repas = "\nRepas : ";
    	String dessert = "\nDessert(s) : ";

    	
    	for (Plat p : this.listePlats) {
    		if (p instanceof Entree){
    			entree += "\n\t- " + p.affichePlat()+" " + p.prix + " � ";
    		}
    		if (p instanceof Boisson){
    			boisson += "\n\t- " + p.affichePlat()+" " + p.prix + " � ";
    		}  
    		if (p instanceof Repas){
    			repas += "\n\t- " + p.affichePlat()+" " + p.prix + " � ";
    		}      		
    		if (p instanceof Dessert){
    			dessert += "\n\t- " + p.affichePlat()+" " + p.prix + " � ";
    		}  
    	}
    	
    	switch (catego) {
		case "Entree":
			return entree;
		case "Plat":
			return repas;
		case "Boisson":
			return boisson;
		case "Dessert":
			return dessert;
		case "Tout":
			return boisson + entree + repas + dessert;
		default:
			break;
		}
		return "error404notfound";

    }

    /**
     * 
     */
    public Vector<Menu> afficheMenus() {
		return listeMenus;

    }

    /**
     * 
     */
    public void chercheMenu(String m) {
        // TODO implement here
    }

	public String afficheMenuDuJour() {
		
		return this.menuDuJour.afficheMenuDetaille();
	}

    /**
     * Return : Vector<Plat> : Une liste contenant toutes les boissons de la carte
     */
    public Vector<Plat> getListeBoisson() {
        
    	Vector<Plat> listeBoisson = new Vector();
    	
    	for (Plat p : this.listePlats) {
    		if (p instanceof Boisson){
    			listeBoisson.addElement(p);
    		}
    		 
    	}
    	return listeBoisson;
    	
    }
    
    /**
     * Return : Vector<Plat> : Une liste contenant toutes les desserts de la carte
     */
    public Vector<Plat> getListeDessert() {
        
    	Vector<Plat> listeRes = new Vector();
    	
    	for (Plat p : this.listePlats) {
    		if (p instanceof Dessert){
    			listeRes.addElement(p);
    		}
    		 
    	}
    	return listeRes;
    	
    }
    
    /**
     * Return : Vector<Plat> : Une liste contenant toutes les plats principaux ( Repas ) de la carte
     */
    public Vector<Plat> getListeRepas() {
        
    	Vector<Plat> listeRes = new Vector();
    	
    	for (Plat p : this.listePlats) {
    		if (p instanceof Repas){
    			listeRes.addElement(p);
    		}
    		 
    	}
    	return listeRes;
    	
    }
    
    /**
     * Return : Vector<Plat> : Une liste contenant toutes les entr�es de la carte
     */
    public Vector<Plat> getListeEntree() {
        
    	Vector<Plat> listeRes = new Vector();
    	
    	for (Plat p : this.listePlats) {
    		if (p instanceof Entree){
    			listeRes.addElement(p);
    		}
    		 
    	}
    	return listeRes;
    	
    }

    /**
     * @return 
     * 
     */
    public Vector<Menu> getListeMenu() {
        return this.listeMenus;
    }

	public void initCarte() {
		// TODO Auto-generated method stub
		this.listePlats = new Vector();
    	this.listeMenus = new Vector();
    	
    	this.menuDuJour = new Menu("Menu du Jour");
    	
    	this.listeMenus.add(new Menu("Grenouille"));
    	this.listeMenus.add(new Menu("Agneau"));
    	
    	this.listePlats.add(new Boisson("Coca", 2.5));
    	this.listePlats.add(new Entree("Tomate Mozarella", 5.0));
    	this.listePlats.add(new Repas("Grenouille", 9.0));
    	this.listePlats.add(new Repas("Patates", 5.0));
    	this.listePlats.add(new Dessert("Tiramisu", 3.0));
    	
    	this.listePlats.add(new Boisson("Orangina", 2.5));
    	this.listePlats.add(new Entree("Salade verte", 5.0));
    	this.listePlats.add(new Repas("Pizza v�g�tarienne", 9.0));
    	this.listePlats.add(new Dessert("Moelleux au chocolat", 3.0));
    	
    	this.listePlats.add(new Boisson("Eau", 2.5));
    	this.listePlats.add(new Boisson("Cafe", 1.5));
    	this.listePlats.add(new Entree("Salade nicoise", 5.0));
    	this.listePlats.add(new Repas("Cotelette d'agneau", 9.0));
    	this.listePlats.add(new Repas("Haricot vert", 9.0));
    	this.listePlats.add(new Dessert("Caf� gourmand", 3.0));

	}
	
}