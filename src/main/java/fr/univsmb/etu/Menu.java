package fr.univsmb.etu;

import java.util.*;

/**
 * 
 */
public class Menu {

    /**
     * 
     */
    public Menu(String n) {
    	this.nom = n;
    	this.listePlats = new Vector();
    	switch(n){
    	case "Grenouille":
    		this.prix = 15.50;
	    	this.listePlats.add(new Boisson("Coca", 2.5));
	    	this.listePlats.add(new Entree("Tomate Mozarella", 5.0));
	    	this.listePlats.add(new Entree("Concombre", 5.0));
	    	this.listePlats.add(new Repas("Grenouille", 9.0));
	    	this.listePlats.add(new Repas("Patates", 5.0));
	    	this.listePlats.add(new Dessert("Tiramisu", 3.0));
	    	break;
    	case "Menu du Jour":
    		this.prix = 12.50;
	    	this.listePlats.add(new Boisson("Orangina", 2.5));
	    	this.listePlats.add(new Entree("Salade verte", 5.0));
	    	this.listePlats.add(new Repas("Pizza v�g�tarienne", 9.0));
	    	this.listePlats.add(new Dessert("Moelleux au chocolat", 3.0));
	    	break;
    	case "Agneau":
    		this.prix = 18;
	    	this.listePlats.add(new Boisson("Eau", 2.5));
	    	this.listePlats.add(new Entree("Salade nicoise", 5.0));
	    	this.listePlats.add(new Repas("Cotelette d'agneau", 9.0));
	    	this.listePlats.add(new Repas("Haricot vert", 9.0));
	    	this.listePlats.add(new Dessert("Caf� gourmand", 3.0));
    	default:
    		break;
    	}
    
    }

    /**
     * 
     */
    public String nom;

    /**
     * 
     */
    public double prix;








    /**
     * 
     */
    public Vector<Plat> listePlats;

    /**
     * 
     */
    
    public String afficheMenuDetaille() {
    	
    	String entree = "\nEntr�e(s) : ";
    	String boisson = "\nBoisson : ";
    	String repas = "\nRepas : ";
    	String dessert = "\nDessert(s) : ";
    	
    	for (Plat p : this.listePlats) {
    		if (p instanceof Entree){
    			entree += "\n\t- " + p.affichePlat()+" ";
    		}
    		if (p instanceof Boisson){
    			boisson += "\n\t- " + p.affichePlat()+" ";
    		}  
    		if (p instanceof Repas){
    			repas += "\n\t- " + p.affichePlat()+" ";
    		}      		
    		if (p instanceof Dessert){
    			dessert += "\n\t- " + p.affichePlat()+" ";
    		}  
    	}
    	
        return "Menu " + this.nom + " : " + this.prix + " � \n" + boisson + entree + repas + dessert;
    }

    /**
     * 
     */
    public String afficheMenus() {
        return this.nom;
    }
    
    /**
     * @return 
     * 
     */
    public Vector<Plat> getListePlat() {
        return listePlats;
    }

}