package fr.univsmb.etu;

public class EtatTicketEnPreparation implements EtatTicket {

	public final Ticket ticket;
	public EtatTicketEnPreparation(Ticket t) {
		// TODO Auto-generated constructor stub
		this.ticket = t;
		
	}	
	
	@Override
	public void aEnvoyer() {
		// TODO Auto-generated method stub

	}

	@Override
	public void aPreparer() {
		// TODO Auto-generated method stub

	}

	@Override
	public void aVenirChercher() {
		// TODO Auto-generated method stub
		ticket.setEtatTicket(new EtatTicketAVenirChercher(ticket));
		ticket.notifierObservateurs();
		ticket.notifyObservers();

	}

	@Override
	public void aRecuperer() {
		// TODO Auto-generated method stub

	}

	@Override
	public void notifierObservateurs() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean estBar() {
		for (Plat p : ticket.listePlats) {
			if (p instanceof Boisson) {
			}
			else {
				return false;
			}
		}
		return true;
		
	}

	@Override
	public boolean estCuisine() {
		for (Plat p : ticket.listePlats) {
			if (p instanceof Boisson) {
				return false;
			}
		}
		return true;
		
	}
	public String toString(){
		return " En Preparation";
	}

}
