package fr.univsmb.etu;


import org.junit.Assert;
import org.junit.Test;


/**
 * Unit test for simple App.
 */
public class TestRestaurant{

    @Test
    public void aNewTableShouldHave0ClientAndBeFree(){
        Table table = new Table(0);
        Assert.assertEquals(table.etat,"libre");
        Assert.assertEquals(table.nb_client,0);
    }

    @Test
    public void theStateOfaNewOrderShouldBeEnCreationAfterAddingAnElement(){
        Commande c = new Commande();
        c.ajoutPlat(new Boisson("coca",2.5),1);
        Assert.assertEquals(c.etat,EtatCommande.EnCreation);
    }


}
